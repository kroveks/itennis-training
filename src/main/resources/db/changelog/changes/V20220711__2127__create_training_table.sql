create table "trainings" (
                         id BIGSERIAL PRIMARY KEY NOT NULL,
                         coach_id BIGINT,
                         training_data timestamp with time zone,
                         capacity int,
                         remaining_places int,
                         duration real,
                         cost real,
                         place VARCHAR(255),
                         type VARCHAR(255),
                         created_at timestamp without time zone,
                         updated_at timestamp without time zone,
                         actual BOOLEAN,
                         coating_type VARCHAR(255)
);
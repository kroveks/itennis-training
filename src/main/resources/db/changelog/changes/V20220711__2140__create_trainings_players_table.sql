create table "trainings_players" (
                           trainings_id BIGINT references trainings(id) ON DELETE CASCADE,
                           user_id BIGINT,
                           created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
                           updated_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
                           actual boolean DEFAULT true,
                           PRIMARY KEY (trainings_id, user_id)
);
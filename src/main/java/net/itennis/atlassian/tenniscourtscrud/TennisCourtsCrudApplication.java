package net.itennis.atlassian.tenniscourtscrud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TennisCourtsCrudApplication {

    public static void main(String[] args) {
        SpringApplication.run(TennisCourtsCrudApplication.class, args);
    }

}

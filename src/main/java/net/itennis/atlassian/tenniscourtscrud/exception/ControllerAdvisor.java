package net.itennis.atlassian.tenniscourtscrud.exception;

import net.itennis.atlassian.tenniscourtscrud.exception.training.TrainingNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

@RestControllerAdvice
public class ControllerAdvisor extends ResponseEntityExceptionHandler {

    @ExceptionHandler(TrainingNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public JsonExceptionResponse handleTrainingNotFoundException(TrainingNotFoundException ex) {
        return new JsonExceptionResponse(LocalDateTime.now(), ex.getMessage());
    }

    @ExceptionHandler(OperationNotSupported.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public JsonExceptionResponse handleOperationNotSupported(OperationNotSupported ex) {
        return new JsonExceptionResponse(LocalDateTime.now(), ex.getMessage());
    }

}
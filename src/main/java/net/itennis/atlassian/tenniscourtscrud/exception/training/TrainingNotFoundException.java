package net.itennis.atlassian.tenniscourtscrud.exception.training;

public class TrainingNotFoundException extends RuntimeException {
    public TrainingNotFoundException(long id) {
        super("Training with id: " + id + " was not found");
    }
}

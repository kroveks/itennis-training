package net.itennis.atlassian.tenniscourtscrud.exception;

public class OperationNotSupported extends RuntimeException{
    public OperationNotSupported() {
        super("Operation is not supported yet");
    }
}

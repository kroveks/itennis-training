package net.itennis.atlassian.tenniscourtscrud.model.composite_key;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TrainingPlayerId implements Serializable {
    public long trainings_id;
    public long user_id;
}

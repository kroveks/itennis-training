package net.itennis.atlassian.tenniscourtscrud.model;

import lombok.*;
import net.itennis.atlassian.tenniscourtscrud.model.composite_key.TrainingPlayerId;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "trainings_players")
public class TrainingPlayer {
    public TrainingPlayer(long trainingId, long playerId) {
        this.trainingPlayerId = new TrainingPlayerId(trainingId, playerId);
    }
    @EmbeddedId
    private TrainingPlayerId trainingPlayerId;
    @Column(name = "created_at")
    private LocalDateTime created_at;
    @Column(name = "updated_at")
    private LocalDateTime updated_at;
    @Column(name = "actual")
    private boolean actual;

    @ManyToOne
    @JoinColumn(name = "training_id")
    private Training training;
}

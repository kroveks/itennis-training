package net.itennis.atlassian.tenniscourtscrud.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "trainings")
public class Training {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;
    @Column(name = "coach_id")
    private long coach_id;

    @Column(name = "training_data")
    private LocalDateTime training_data;
    @Column(name = "capacity")
    private int capacity;
    @Column(name = "remaining_places")
    private int remaining_places;
    @Column(name = "duration")
    private double duration;
    @Column(name = "cost")
    private double cost;
    @Column(name = "place")
    private String place;
    @Column(name = "type")
    private String type;
    @Column(name = "created_at")
    private LocalDateTime created_at;
    @Column(name = "updated_at")
    private LocalDateTime updated_at;
    @Column(name = "actual")
    private boolean actual;
    @Column(name = "coating_type")
    private String coating_type;
}

package net.itennis.atlassian.tenniscourtscrud.repository;

import net.itennis.atlassian.tenniscourtscrud.model.Training;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TrainingRepository extends PagingAndSortingRepository<Training, Long>,
                                            JpaSpecificationExecutor<Training> {
    @Modifying
    @Query("update Training t set t.remaining_places = t.remaining_places - 1 where t.id = :trainingId")
    void decreaseRemainingPlacesById(long trainingId);
}

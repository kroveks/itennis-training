package net.itennis.atlassian.tenniscourtscrud.repository;

import net.itennis.atlassian.tenniscourtscrud.model.TrainingPlayer;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TrainingPlayerRepository extends PagingAndSortingRepository<TrainingPlayer, Long>,
                                                  JpaSpecificationExecutor<TrainingPlayer> {
}

package net.itennis.atlassian.tenniscourtscrud.service;

import net.itennis.atlassian.tenniscourtscrud.model.Training;

import java.util.List;

public interface TrainingService {
    List<Training> getAllTrainings(int page, int size);
    Training getTrainingById(long id);
    long createTraining(Training training);
    void addPlayer(long trainingId, long playerId);
    void updateTraining(Training training);
    void deleteTrainingById(long id);
}
package net.itennis.atlassian.tenniscourtscrud.service.impl;

import net.itennis.atlassian.tenniscourtscrud.exception.training.TrainingNotFoundException;
import net.itennis.atlassian.tenniscourtscrud.model.Training;
import net.itennis.atlassian.tenniscourtscrud.model.TrainingPlayer;
import net.itennis.atlassian.tenniscourtscrud.repository.TrainingPlayerRepository;
import net.itennis.atlassian.tenniscourtscrud.repository.TrainingRepository;
import net.itennis.atlassian.tenniscourtscrud.service.TrainingService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TrainingServiceImpl implements TrainingService {

    private final static Logger logger = LogManager.getLogger(TrainingServiceImpl.class);
    private final TrainingRepository trainingRepository;
    private final TrainingPlayerRepository trainingPlayerRepository;

    public TrainingServiceImpl(TrainingRepository trainingRepository,
                               TrainingPlayerRepository trainingPlayerRepository)
    {
        this.trainingRepository = trainingRepository;
        this.trainingPlayerRepository = trainingPlayerRepository;
    }

    @Override
    public List<Training> getAllTrainings(int page, int size) {
        List<Training> trainingList = new ArrayList<>();
        logger.info("Trying to fetch list of trainings");
        // default "size" value is 0, so it means that with default parameters we fetch all users
        if (size == 0) {
            trainingRepository.findAll()
                              .forEach(trainingList::add);
        } else {
            trainingRepository.findAll(PageRequest.of(page, size))
                              .forEach(trainingList::add);
        }
        logger.info("{} trainings were fetched", trainingList.size());
        return trainingList;
    }

    @Override
    public Training getTrainingById(long id) {
        logger.info("Trying to fetch training with id: {}", id);
        Optional<Training> trainingOptional = trainingRepository.findById(id);
        if (trainingOptional.isPresent()) {
            logger.info("Training with id: {} was successfully fetched", id);
            return trainingOptional.get();
        } else {
            logger.error("Training with id: {} was not found", id);
            throw new TrainingNotFoundException(id);
        }
    }

    @Override
    @Transactional
    public long createTraining(Training training) {
        logger.info("Trying to create training with coach_id: {}", training.getCoach_id());
        Training savedTraining = trainingRepository.save(training);
        long id = savedTraining.getId();
        logger.info("Training with id: {} created", id);
        return id;

    }

    @Override
    @Transactional
    public void addPlayer(long trainingId, long playerId) {
        logger.info("Trying to add player with id: " + playerId + " to the training with id " + trainingId);
        Optional<Training> trainingOptional = trainingRepository.findById(trainingId);
        if (trainingOptional.isPresent()) {
            trainingRepository.decreaseRemainingPlacesById(trainingId);
            trainingPlayerRepository.save(new TrainingPlayer(trainingId, playerId));
            logger.info("Player with id: " + playerId + " was successfully added to the training with id " + trainingId);
        } else {
            logger.error("Training with id: {} was not found", trainingId);
            throw new TrainingNotFoundException(trainingId);
        }
    }

    @Override
    @Transactional
    public void updateTraining(Training training) {
        long id = training.getId();
        logger.info("Trying to update training with id: {}", id);
        trainingRepository.save(training);
        logger.info("Training with id: {} was successfully updated", id);
    }

    @Override
    @Transactional
    public void deleteTrainingById(long id) {
        logger.info("Trying to delete training with id: {}", id);
        trainingRepository.deleteById(id);
        logger.info("Training with id: {} successfully deleted", id);
    }
}
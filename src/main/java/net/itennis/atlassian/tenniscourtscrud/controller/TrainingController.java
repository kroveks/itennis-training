package net.itennis.atlassian.tenniscourtscrud.controller;

import net.itennis.atlassian.tenniscourtscrud.model.Training;
import net.itennis.atlassian.tenniscourtscrud.service.TrainingService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/trainings")
public class TrainingController {

    private final static Logger logger = LogManager.getLogger(TrainingController.class);
    private final TrainingService trainingService;

    public TrainingController(TrainingService trainingService) {
        this.trainingService = trainingService;
    }

    @PostMapping("/")
    public ResponseEntity<Long> createTraining(@RequestBody Training training) {
        logger.info("Got training: " + training);
        Long id = trainingService.createTraining(training);
        return new ResponseEntity<>(id, HttpStatus.CREATED);
    }

    @PostMapping("/addPlayer")
    public ResponseEntity<Training> addPlayer(
            @RequestParam(name = "playerId") long playerId,
            @RequestParam(name = "trainingId") long trainingId
    ) {
        logger.info("Got player id: " + playerId + " and training id: " + trainingId);
        trainingService.addPlayer(trainingId, playerId);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
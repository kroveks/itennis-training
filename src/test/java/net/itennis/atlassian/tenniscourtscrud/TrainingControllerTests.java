package net.itennis.atlassian.tenniscourtscrud;

import net.itennis.atlassian.tenniscourtscrud.model.Training;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class TrainingControllerTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    @Order(0)
    void contextLoads() {
    }

    @Test
    @Order(1)
    void createTrainingTest() {
        Training training = createTraining();

        ResponseEntity<Long> response = restTemplate.postForEntity("/trainings/", training, Long.class);

        assertThat(response.getStatusCode(), is(HttpStatus.CREATED));
        assertThat(response.getBody(), notNullValue());
    }

    @Test
    @Order(2)
    void addPlayerTest() {
        ResponseEntity<Training> response = restTemplate.exchange(
                                        "/trainings/addPlayer?playerId=1&trainingId=1",
                                        HttpMethod.POST,
                            null,
                                        new ParameterizedTypeReference<>() {}
        );
        assertThat(response.getStatusCode(), is(HttpStatus.CREATED));
    }


    private Training createTraining() {
        return Training.builder()
                .coach_id(1L)
                .training_data(LocalDateTime.now())
                .capacity(10)
                .remaining_places(10)
                .duration(1.5)
                .cost(1000.0)
                .place("Court")
                .type("Training")
                .created_at(LocalDateTime.now())
                .updated_at(LocalDateTime.now())
                .actual(true)
                .coating_type("Grass")
                .build();
    }
}